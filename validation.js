const validatePostBody = (req, res, next) => {
  if (!req.body.calories) {
    const err = new Error('Missing calories');
    err.status = 400;
    next(err);
  }

  if (!/^\d*$/.test(req.body.calories)) {
    const err = new Error('Calories must be a number');
    err.status = 400;
    next(err);
  }

  next();
};

const passwordValidation = (req, res, next) => {
  const { password, confirmPassword } = req.body;

  if (!password) {
    const err = new Error('Missing password');
    err.status = 400;
    next(err);
  }

  if (password.length < 8) {
    const err = new Error('Password must be at least 8 characters');
    err.status = 400;
    next(err);
  }

  if (password !== confirmPassword) {
    const err = new Error('Passwords do not match');
    err.status = 400;
    next(err);
  }

  next();
};

module.exports = { validatePostBody, passwordValidation };
