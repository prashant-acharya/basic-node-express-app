const fs = require('fs');
const express = require('express');
const { validatePostBody } = require('./validation');

const app = express();

app.use(express.text());
app.use(express.json());

app.use((req, res, next) => {
  console.log('Request Received', req.url, req.method);

  next();
});

app.get('/', (req, res) => {
  res.send({ message: 'Hello World' });
});

app.post('/calories', validatePostBody, (req, res) => {
  const { calories } = req.body;

  // write async
  fs.writeFileSync('calories.txt', `${calories}`);

  res.status(201).send({ message: `Saved ${calories} calories` });
});

app.get('/calories', (req, res, next) => {
  const calories = fs.readFileSync('calories.txt', 'utf8');

  res.send({ message: `You have ${calories} calories` });
});

app.use((req, res, next) => {
  const err = new Error('Page not found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  const statusCode = err.status || 500;
  res.status(statusCode).send({ message: err.message });
});

app.listen(process.env.PORT);
